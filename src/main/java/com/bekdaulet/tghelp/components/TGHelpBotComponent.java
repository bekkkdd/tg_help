package com.bekdaulet.tghelp.components;

import com.bekdaulet.tghelp.BotConfig;
import com.bekdaulet.tghelp.domain.goods.model.Help;
import com.bekdaulet.tghelp.domain.goods.model.Need;
import com.bekdaulet.tghelp.domain.goods.services.HelpsService;
import com.bekdaulet.tghelp.domain.goods.services.NeedsService;
import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import com.bekdaulet.tghelp.domain.user.model.Town;
import com.bekdaulet.tghelp.domain.user.service.RegionService;
import com.bekdaulet.tghelp.domain.user.service.TelegramAccountService;
import com.bekdaulet.tghelp.domain.user.service.TownService;
import com.bekdaulet.tghelp.utils.TelegramBotUtil;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Chat;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.*;
import com.pengrad.telegrambot.response.BaseResponse;
import com.pengrad.telegrambot.response.SendResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.bekdaulet.tghelp.domain.user.model.TelegramAccountStatus.*;

@Component
public class TGHelpBotComponent {

    @Autowired
    TelegramAccountService telegramAccountService;
    @Autowired
    RegionService regionService;
    @Autowired
    TownService townService;
    @Autowired
    NeedsService needsService;
    @Autowired
    HelpsService helpsService;

    private String token = BotConfig.BOT_TOKEN;
    private TelegramBot bot;
    private TelegramBotUtil botUtil;

    public SendResponse sendMessage(String message, long chatId) {
        SendResponse sendResponse = botUtil.sendMessage(bot, chatId, message);
        return sendResponse;
    }

    public SendResponse sendMessage(String message, long chatId, Keyboard replyMarkup) {
        SendResponse sendResponse = botUtil.sendMessage(bot, chatId, message, replyMarkup);
        return sendResponse;
    }

    public SendResponse sendMessage(String message, long chatId, Keyboard replyMarkup, int replyToMessageId) {
        SendResponse sendResponse = botUtil.sendMessage(bot, chatId, message, replyMarkup, replyToMessageId);
        return sendResponse;
    }

    public SendResponse sendMessage(String message, long chatId, Keyboard replyMarkup, Boolean html, Boolean silent, Boolean preview) {
        SendResponse sendResponse = botUtil.sendMessage(bot, chatId, message, html, silent, preview, replyMarkup);
        return sendResponse;
    }

    @PostConstruct
    public void botStart() {
        bot = new TelegramBot(token);
//        bot.execute()
        botUtil = new TelegramBotUtil();
        bot.setUpdatesListener(updates -> {
            for (Update update : updates) {
                try {
                    Message message = update.callbackQuery() == null ? (update.message() == null ? update.editedMessage() : update.message()) : update.callbackQuery().message();
                    Chat telegramChat = message.chat();
                    Long chatId = telegramChat.id();
                    System.out.println(message.toString());
                    String text = message.text();
                    TelegramAccount telegramAccount = telegramAccountService.findFirstByAccountId(String.valueOf(message.chat().id()));
                    if (update.callbackQuery() != null && update.callbackQuery().data() != null) {
                        text = update.callbackQuery().data();
                        System.out.println("Callback " + text);
                        if (text.contains("/setRegion") && telegramAccount.getTelegramAccountStatus() == CHOOSE_REGION) {
                            System.out.println();
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Region region = regionService.findFirstById(id);
                                if (region != null) {
                                    telegramAccount.setRegion(region);
                                    telegramAccount.setTelegramAccountStatus(CHOOSE_TOWN);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }
                        } else if (text.contains("/setTown") && telegramAccount.getTelegramAccountStatus() == CHOOSE_TOWN) {
                            System.out.println();
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Town town = townService.findFirstById(id);
                                if (town != null) {
                                    telegramAccount.setTown(town);
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }
                        } else if (text.contains("/iCanHelp") && telegramAccount.getTelegramAccountStatus() == MENU) {
                            telegramAccount.setTelegramAccountStatus(I_CAN_HELP);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/iNeedHelp") && telegramAccount.getTelegramAccountStatus() == MENU) {
                            telegramAccount.setTelegramAccountStatus(I_NEED_HELP);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/helpersList") && telegramAccount.getTelegramAccountStatus() == MENU) {
                            telegramAccount.setTelegramAccountStatus(WHO_CAN_HELP);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/neediesList") && telegramAccount.getTelegramAccountStatus() == MENU) {
                            telegramAccount.setTelegramAccountStatus(WHO_NEED_HELP);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/listMyHelps") && telegramAccount.getTelegramAccountStatus() == MENU) {
                            telegramAccount.setTelegramAccountStatus(LIST_MY_HELPS);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/deleteMyHelp") && telegramAccount.getTelegramAccountStatus() == LIST_MY_HELPS) {
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Help help = helpsService.findFirstById(id);
                                if (help != null) {
                                    help.setSoftDeleted(true);
                                    helpsService.save(help);
                                    telegramAccount.setTelegramAccountStatus(LIST_MY_HELPS);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }

                        } else if (text.contains("/listMyNeeds") && telegramAccount.getTelegramAccountStatus() == MENU) {
                            telegramAccount.setTelegramAccountStatus(LIST_MY_NEEDS);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/deleteMyNeed") && telegramAccount.getTelegramAccountStatus() == LIST_MY_NEEDS) {
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Need need = needsService.findFirstById(id);
                                if (need != null) {
                                    need.setSoftDeleted(true);
                                    needsService.save(need);
                                    telegramAccount.setTelegramAccountStatus(LIST_MY_NEEDS);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }

                        } else if (text.contains("/searchHelpersByRegion") && telegramAccount.getTelegramAccountStatus() == WHO_CAN_HELP) {
                            telegramAccount.setTelegramAccountStatus(SEARCH_HELPERS_BY_REGION);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/searchHelpersByKeywords") && telegramAccount.getTelegramAccountStatus() == WHO_CAN_HELP) {
                            telegramAccount.setTelegramAccountStatus(SEARCH_HELPERS_BY_KEYWORDS);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/searchNeediesByRegion") && telegramAccount.getTelegramAccountStatus() == WHO_NEED_HELP) {
                            telegramAccount.setTelegramAccountStatus(SEARCH_NEEDIES_BY_REGION);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/searchNeediesByKeywords") && telegramAccount.getTelegramAccountStatus() == WHO_NEED_HELP) {
                            telegramAccount.setTelegramAccountStatus(SEARCH_NEEDIES_BY_KEYWORDS);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/searchHelpersByTown") && telegramAccount.getTelegramAccountStatus() == SEARCH_HELPERS_BY_REGION) {
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Region searchRegion = regionService.findFirstById(id);
                                if (searchRegion != null) {
                                    telegramAccount.setSearchRegion(searchRegion);
                                    telegramAccount.setTelegramAccountStatus(SEARCH_HELPERS_BY_TOWN);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }
                        } else if (text.contains("/searchNeediesByTown") && telegramAccount.getTelegramAccountStatus() == SEARCH_NEEDIES_BY_REGION) {
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Region searchRegion = regionService.findFirstById(id);
                                if (searchRegion != null) {
                                    telegramAccount.setSearchRegion(searchRegion);
                                    telegramAccount.setTelegramAccountStatus(SEARCH_NEEDIES_BY_TOWN);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }
                        } else if (text.contains("/showTownHelpers") && telegramAccount.getTelegramAccountStatus() == SEARCH_HELPERS_BY_TOWN) {
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Town searchTown = townService.findFirstById(id);
                                if (searchTown != null) {
                                    telegramAccount.setPage(0);
                                    telegramAccount.setSearchTown(searchTown);
                                    telegramAccount.setTelegramAccountStatus(SHOW_HELPERS_BY_TOWN);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }
                        } else if (text.contains("/showTownNeedies") && telegramAccount.getTelegramAccountStatus() == SEARCH_NEEDIES_BY_TOWN) {
                            try {
                                Long id = Long.valueOf(text.substring(text.lastIndexOf('/') + 1));
                                Town searchTown = townService.findFirstById(id);
                                if (searchTown != null) {
                                    telegramAccount.setPage(0);
                                    telegramAccount.setSearchTown(searchTown);
                                    telegramAccount.setTelegramAccountStatus(SHOW_NEEDIES_BY_TOWN);
                                    telegramAccountService.save(telegramAccount);
                                }
                            } catch (Exception e) {
                                sendMessage("Внутренняя ошибка сервера. Повторите попытку.", chatId);
                            }
                        } else if (text.contains("/decrementPage")) {
                            telegramAccount.setPage(telegramAccount.getPage() - 1);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.contains("/incrementPage")) {
                            telegramAccount.setPage(telegramAccount.getPage() + 1);
                            telegramAccountService.save(telegramAccount);
                        } else if (text.equals("/back")) {
                            switch (telegramAccount.getTelegramAccountStatus()) {
                                case SEARCH_NEEDIES_BY_KEYWORDS:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SEARCH_HELPERS_BY_KEYWORDS:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SHOW_NEEDIES_BY_KEYWORDS:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SHOW_HELPERS_BY_KEYWORDS:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SEARCH_NEEDIES_BY_REGION:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SEARCH_HELPERS_BY_REGION:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SEARCH_NEEDIES_BY_TOWN:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SEARCH_HELPERS_BY_TOWN:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SHOW_NEEDIES_BY_TOWN:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case SHOW_HELPERS_BY_TOWN:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case I_CAN_HELP:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case I_NEED_HELP:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case WHO_CAN_HELP:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case WHO_NEED_HELP:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case LIST_MY_HELPS:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case LIST_MY_NEEDS:
                                    telegramAccount.setTelegramAccountStatus(MENU);
                                    break;
                                case MENU:
                                    telegramAccount.setTelegramAccountStatus(CHOOSE_TOWN);
                                    break;
                                case CHOOSE_TOWN:
                                    telegramAccount.setTelegramAccountStatus(CHOOSE_REGION);
                                    break;
                                case CHOOSE_REGION:
                                    telegramAccount.setTelegramAccountStatus(FIO);
                                    break;
                                case FIO:
                                    telegramAccount.setTelegramAccountStatus(PHONE_NUMBER);
                                    break;
                                default:
                                    break;
                            }
                            telegramAccountService.save(telegramAccount);
                        }
                        System.out.println(text + " From @" + message.chat().username());
                        text = null;
                    }
                    System.out.println(text + " From @" + message.chat().username());

                    if (telegramAccount == null) {
                        telegramAccount = new TelegramAccount();
                        telegramAccount.setAccountId(String.valueOf(message.chat().id()));
                        telegramAccount.setChatId(chatId.toString());
                        telegramAccount.setTelegramAccountStatus(PHONE_NUMBER);
                        telegramAccount.setUsername(message.chat().username());
                        telegramAccountService.save(telegramAccount);
                    }

                    System.out.println(telegramAccount.getTelegramAccountStatus().toString());
                    if (telegramAccount.getTelegramAccountStatus() == PHONE_NUMBER) {
                        if (text != null && text.contains("87")) {
                            if (text.trim().matches("87[047]\\d{8}")) {
                                String number = text.trim();
                                telegramAccount.setPhoneNumber(number);
                                telegramAccount.setTelegramAccountStatus(FIO);
                                telegramAccountService.save(telegramAccount);
                                sendMessage("Номер " + number + " успешно зарегался\nВведите ФИО:", chatId);
                            } else {
                                sendMessage("Введите в корректной форме", chatId);
                            }
                        } else {
                            SendResponse sendResponse = sendMessage("Отправь номер телефона в формате 87ХХХХХХХХХ", chatId);
                        }
                    } else if (telegramAccount.getTelegramAccountStatus() == FIO) {
                        if (text != null) {
                            text = text.trim();
                            try {
                                String firstLetter = String.valueOf(text.charAt(0));
                                String secondLetter = String.valueOf(text.charAt(text.indexOf(' ') + 1));

                                if (text.length() > 4 && firstLetter.equals(firstLetter.toUpperCase()) && secondLetter.equals(secondLetter.toUpperCase())) {
                                    telegramAccount.setFullName(text);
                                    telegramAccount.setTelegramAccountStatus(CHOOSE_REGION);
                                    telegramAccountService.save(telegramAccount);
                                    sendMessage("ФИО " + text + " успешно записано", chatId);
                                } else {
                                    throw new Exception("Custom Error");
                                }
                            } catch (Exception e) {
                                sendMessage("Введите ФИО Корректно", chatId);
                            }
                        }
                    }
                    if (telegramAccount.getTelegramAccountStatus() == CHOOSE_REGION) {
                        List<Region> regions = regionService.findAll();
                        InlineKeyboardButton[][] regionButtons = new InlineKeyboardButton[regions.size()][1];
                        int i = 0;
                        for (Region region : regions) {
                            regionButtons[i][0] = new InlineKeyboardButton(region.getName()).callbackData("/setRegion/" + region.getId());
                            i++;
                        }
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(regionButtons);
                        System.out.println(inlineKeyboardMarkup.toString());
                        sendMessage("Выберите ваш регион", chatId, inlineKeyboardMarkup);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == CHOOSE_TOWN) {
                        if (telegramAccount.getRegion() != null) {
                            List<Town> towns = townService.findAllByRegion(telegramAccount.getRegion());
                            InlineKeyboardButton[][] townButtons = new InlineKeyboardButton[towns.size()][1];
                            int i = 0;
                            for (Town town : towns) {
                                townButtons[i][0] = new InlineKeyboardButton(town.getName()).callbackData("/setTown/" + town.getId());
                                i++;
                            }
                            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(townButtons);
                            System.out.println(inlineKeyboardMarkup.toString());
                            sendMessage("Выберите ваш город", chatId, inlineKeyboardMarkup);
                        }
                    }
                    if (telegramAccount.getTelegramAccountStatus() == I_CAN_HELP) {
                        if (text != null && text.trim().length() > 9) {
                            telegramAccount.setHelper(true);
                            telegramAccountService.save(telegramAccount);
                            Help help = new Help();
                            help.setName(text);
                            help.setCanHelpAccount(telegramAccount);
                            helpsService.save(help);
                            sendMessage(text + "\n-------------\nУспешно записан", chatId);
                            telegramAccount.setTelegramAccountStatus(MENU);
                            telegramAccountService.save(telegramAccount);
                        } else if (text != null) {
                            sendMessage("Пожалуйста, Напишите более подробно чем вы можете помочь", chatId);
                        } else {
                            sendMessage("Напишите, чем вы можете помочь", chatId);
                        }
                    }
                    if (telegramAccount.getTelegramAccountStatus() == I_NEED_HELP) {
                        if (text != null && text.trim().length() > 9) {
                            telegramAccount.setNeedy(true);
                            telegramAccountService.save(telegramAccount);
                            Need need = new Need();
                            need.setName(text);
                            need.setNeedHelpAccount(telegramAccount);
                            needsService.save(need);
                            sendMessage(text + "\n-------------\nУспешно записан", chatId);
                            telegramAccount.setTelegramAccountStatus(MENU);
                            telegramAccountService.save(telegramAccount);
                        } else if (text != null) {
                            sendMessage("Пожалуйста, Напишите более подробно в чем вы нуждаетесь", chatId);
                        } else {
                            sendMessage("Напишите, в чем вы нуждаетесь", chatId);
                        }
                    }
                    if (telegramAccount.getTelegramAccountStatus() == WHO_CAN_HELP) {
                        InlineKeyboardButton[][] chooseSearchType = new InlineKeyboardButton[2][1];
                        chooseSearchType[0][0] = new InlineKeyboardButton("Искать по географическому месту").callbackData("/searchHelpersByRegion");
                        chooseSearchType[1][0] = new InlineKeyboardButton("Искать по ключевым словам").callbackData("/searchHelpersByKeywords");
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(chooseSearchType);
                        System.out.println(inlineKeyboardMarkup.toString());
                        sendMessage("Выберите Тип поиска", chatId, inlineKeyboardMarkup);
                    }
                    if(telegramAccount.getTelegramAccountStatus() == SEARCH_HELPERS_BY_KEYWORDS){
                        if (text != null ) {
                            if (text.trim().length() > 2) {
                                String searchString = text.trim();
                                System.out.println(telegramAccount.getFullName() + " is loking for" + searchString);
                                telegramAccount.setSearchString(text);
                                telegramAccount.setPage(0);
                                telegramAccount.setTelegramAccountStatus(SHOW_HELPERS_BY_KEYWORDS);
                                telegramAccountService.save(telegramAccount);
                            } else {
                                sendMessage("Введите в корректной форме", chatId);
                            }
                        } else {
                            SendResponse sendResponse = sendMessage("Введите поиск корректно", chatId);
                        }
                    }
                    if(telegramAccount.getTelegramAccountStatus() == SHOW_HELPERS_BY_KEYWORDS){
                        String searchString = telegramAccount.getSearchString();
                        //Danik
                        List<Help> searchHelps = helpsService.findAll();
                        showHelpsWithPagination(searchHelps, telegramAccount);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == SEARCH_HELPERS_BY_REGION) {
                        List<Region> regions = regionService.findAll();
                        InlineKeyboardButton[][] regionButtons = new InlineKeyboardButton[regions.size()][1];
                        int i = 0;
                        for (Region region : regions) {
                            regionButtons[i][0] = new InlineKeyboardButton(region.getName()).callbackData("/searchHelpersByTown/" + region.getId());
                            i++;
                        }
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(regionButtons);
                        System.out.println(inlineKeyboardMarkup.toString());
                        sendMessage("Выберите регион поиска помощи", chatId, inlineKeyboardMarkup);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == SEARCH_HELPERS_BY_TOWN) {
                        List<Town> towns = townService.findAllByRegion(telegramAccount.getSearchRegion());
                        InlineKeyboardButton[][] regionButtons = new InlineKeyboardButton[towns.size()][1];
                        int i = 0;
                        for (Town town : towns) {
                            regionButtons[i][0] = new InlineKeyboardButton(town.getName()).callbackData("/showTownHelpers/" + town.getId());
                            i++;
                        }
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(regionButtons);
                        System.out.println(inlineKeyboardMarkup.toString());
                        sendMessage("Выберите город поиска помощи", chatId, inlineKeyboardMarkup);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == SHOW_HELPERS_BY_TOWN) {
                        List<Help> helpsByTown = helpsService.findAllByCanHelpAccountIsNotAndCanHelpAccount_Town(telegramAccount, telegramAccount.getSearchTown());
                        showHelpsWithPagination(helpsByTown, telegramAccount);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == WHO_NEED_HELP) {
                        InlineKeyboardButton[][] chooseSearchType = new InlineKeyboardButton[2][1];
                        chooseSearchType[0][0] = new InlineKeyboardButton("Искать по географическому месту").callbackData("/searchNeediesByRegion");
                        chooseSearchType[1][0] = new InlineKeyboardButton("Искать по ключевым словам").callbackData("/searchNeediesByKeywords");
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(chooseSearchType);
                        System.out.println(inlineKeyboardMarkup.toString());
                        sendMessage("Выберите Тип поиска", chatId, inlineKeyboardMarkup);
                    }
                    if(telegramAccount.getTelegramAccountStatus() == SEARCH_NEEDIES_BY_KEYWORDS){
                        if (text != null ) {
                            if (text.trim().length() > 2) {
                                String searchString = text.trim();
                                System.out.println(telegramAccount.getFullName() + " is loking for" + searchString);
                                telegramAccount.setSearchString(text);
                                telegramAccount.setPage(0);
                                telegramAccount.setTelegramAccountStatus(SHOW_NEEDIES_BY_KEYWORDS);
                                telegramAccountService.save(telegramAccount);
                            } else {
                                sendMessage("Введите в корректной форме", chatId);
                            }
                        } else {
                            SendResponse sendResponse = sendMessage("Введите поиск корректно", chatId);
                        }
                    }
                    if(telegramAccount.getTelegramAccountStatus() == SHOW_NEEDIES_BY_KEYWORDS){
                        String searchString = telegramAccount.getSearchString();
                        //Danik
                        List<Need> searchNeeds = needsService.findAll();
                        showNeedsWithPagination(searchNeeds, telegramAccount);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == SEARCH_NEEDIES_BY_REGION) {
                        List<Region> regions = regionService.findAll();
                        InlineKeyboardButton[][] regionButtons = new InlineKeyboardButton[regions.size()][1];
                        int i = 0;
                        for (Region region : regions) {
                            regionButtons[i][0] = new InlineKeyboardButton(region.getName()).callbackData("/searchNeediesByTown/" + region.getId());
                            i++;
                        }
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(regionButtons);
                        System.out.println(inlineKeyboardMarkup.toString());
                        sendMessage("Выберите регион поиска помощи", chatId, inlineKeyboardMarkup);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == SEARCH_NEEDIES_BY_TOWN) {
                        List<Town> towns = townService.findAllByRegion(telegramAccount.getSearchRegion());
                        InlineKeyboardButton[][] regionButtons = new InlineKeyboardButton[towns.size()][1];
                        int i = 0;
                        for (Town town : towns) {
                            regionButtons[i][0] = new InlineKeyboardButton(town.getName()).callbackData("/showTownNeedies/" + town.getId());
                            i++;
                        }
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(regionButtons);
                        System.out.println(inlineKeyboardMarkup.toString());
                        sendMessage("Выберите город поиска помощи", chatId, inlineKeyboardMarkup);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == SHOW_NEEDIES_BY_TOWN) {
                        List<Need> needsByTown = needsService.findAllByNeedHelpAccountIsNotAndNeedHelpAccount_Town(telegramAccount, telegramAccount.getSearchTown());
                        showNeedsWithPagination(needsByTown, telegramAccount);
                    }
                    if (telegramAccount.getTelegramAccountStatus() == LIST_MY_HELPS) {
                        List<Help> myHelps = helpsService.findAllByCanHelpAccount_AccountId(telegramAccount.getAccountId());
                        if (myHelps.size() == 0) {
                            sendMessage("Пусто", chatId);
                            telegramAccount.setTelegramAccountStatus(MENU);
                            telegramAccountService.save(telegramAccount);
                        } else
                            for (Help h :
                                    myHelps) {
                                InlineKeyboardButton[][] townButtons = new InlineKeyboardButton[1][1];
                                townButtons[0][0] = new InlineKeyboardButton("Удалить").callbackData("/deleteMyHelp/" + h.getId());
                                InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(townButtons);
                                sendMessage(h.getName(), chatId, inlineKeyboardMarkup);
                            }
                    }
                    if (telegramAccount.getTelegramAccountStatus() == LIST_MY_NEEDS) {
                        List<Need> myNeeds = needsService.findAllByNeedHelpAccount_AccountId(telegramAccount.getAccountId());
                        if (myNeeds.size() == 0) {
                            sendMessage("Пусто", chatId);
                            telegramAccount.setTelegramAccountStatus(MENU);
                            telegramAccountService.save(telegramAccount);
                        } else
                            for (Need n :
                                    myNeeds) {
                                InlineKeyboardButton[][] townButtons = new InlineKeyboardButton[1][1];
                                townButtons[0][0] = new InlineKeyboardButton("Удалить").callbackData("/deleteMyNeed/" + n.getId());
                                InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(townButtons);
                                sendMessage(n.getName(), chatId, inlineKeyboardMarkup);
                            }
                    }
                    if (telegramAccount.getTelegramAccountStatus() == MENU) {
                        InlineKeyboardButton[][] menuButtons = new InlineKeyboardButton[6][1];
                        menuButtons[0][0] = new InlineKeyboardButton("Я могу помочь!").callbackData("/iCanHelp");
                        menuButtons[1][0] = new InlineKeyboardButton("Мне нужна помощь...").callbackData("/iNeedHelp");
                        menuButtons[2][0] = new InlineKeyboardButton("Список людей, кто может помочь").callbackData("/helpersList");
                        menuButtons[3][0] = new InlineKeyboardButton("Список людей, кому нужна помощь").callbackData("/neediesList");
                        menuButtons[4][0] = new InlineKeyboardButton("Мои помощи").callbackData("/listMyHelps");
                        menuButtons[5][0] = new InlineKeyboardButton("Мои запросы помощи").callbackData("/listMyNeeds");
                        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup(menuButtons);
                        sendMessage("Меню", chatId, inlineKeyboardMarkup);
                    }
                    if (telegramAccount.getTelegramAccountStatus() != PHONE_NUMBER) {
                        InlineKeyboardButton[] back = new InlineKeyboardButton[1];
                        back[0] = new InlineKeyboardButton("\u2190").callbackData("/back");
                        InlineKeyboardMarkup backMarkup = new InlineKeyboardMarkup(back);
                        BaseResponse baseResponse = sendMessage("Назад",
                                chatId,
                                backMarkup);
                        System.out.println(baseResponse.description());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });
    }

    private void showNeedsWithPagination(List<Need> needs, TelegramAccount telegramAccount){
        int needsCount = needs.size();
        int maxPage = needsCount % 10 > 0 ? needsCount / 10 + 1 : needsCount / 10;
        boolean isLastPage = telegramAccount.getPage() + 1 == maxPage;
        boolean isFirstPage = telegramAccount.getPage() == 0;

        if (needs.size() == 0) {
            sendMessage("Пусто", Long.parseLong(telegramAccount.getChatId()));
            telegramAccount.setTelegramAccountStatus(MENU);
            telegramAccountService.save(telegramAccount);
        } else {
            try {
                int fromIndex = telegramAccount.getPage() * 10;
                int toIndex;
                if (needsCount - fromIndex >= 10) {
                    toIndex = fromIndex + 10;
                } else {
                    toIndex = fromIndex + needsCount % 10;
                }
                needs = needs.subList(fromIndex, toIndex);
            } catch (Exception e) {
                telegramAccount.setPage(0);
                telegramAccountService.save(telegramAccount);
            }

            StringBuilder needersByTown = new StringBuilder();
            int i = 1;
            InlineKeyboardMarkup inlineKeyboardMarkup;
            if (!isLastPage && !isFirstPage) {
                InlineKeyboardButton[][] paginationButtons = new InlineKeyboardButton[1][2];
                paginationButtons[0][0] = new InlineKeyboardButton("\u2190Пред.").callbackData("/decrementPage");
                paginationButtons[0][1] = new InlineKeyboardButton("След.\u2192").callbackData("/incrementPage");
                inlineKeyboardMarkup = new InlineKeyboardMarkup(paginationButtons);
            } else {
                InlineKeyboardButton[][] paginationButtons = new InlineKeyboardButton[1][1];
                paginationButtons[0][0] = isLastPage ? new InlineKeyboardButton("\u2190Пред.").callbackData("/decrementPage") : new InlineKeyboardButton("\u2192След.").callbackData("/incrementPage");
                inlineKeyboardMarkup = new InlineKeyboardMarkup(paginationButtons);
            }
            for (Need n :
                    needs) {
//                                sendMessage(h.getName(), chatId, inlineKeyboardMarkup);
                needersByTown.append(i++)
                        .append(") ")
                        .append(n.getName())
                        .append("\nКонтакты:\n")
                        .append(n.getNeedHelpAccount().getFullName())
                        .append("\n")
                        .append(n.getNeedHelpAccount().getPhoneNumber())
                        .append("\n------------\n");
            }
            sendMessage(needersByTown.toString(), Long.parseLong(telegramAccount.getChatId()), inlineKeyboardMarkup);
        }
    }
    private void showHelpsWithPagination(List<Help> helpsList, TelegramAccount telegramAccount){
        int helpsCount = helpsList.size();
        int maxPage = helpsCount % 10 > 0 ? helpsCount / 10 + 1 : helpsCount / 10;
        boolean isLastPage = telegramAccount.getPage() + 1 == maxPage;
        boolean isFirstPage = telegramAccount.getPage() == 0;

        if (helpsList.size() == 0) {
            sendMessage("Пусто", Long.parseLong(telegramAccount.getChatId()));
            telegramAccount.setTelegramAccountStatus(MENU);
            telegramAccountService.save(telegramAccount);
        } else {
            try {
                int fromIndex = telegramAccount.getPage() * 10;
                int toIndex;
                if (helpsCount - fromIndex >= 10) {
                    toIndex = fromIndex + 10;
                } else {
                    toIndex = fromIndex + helpsCount % 10;
                }
                helpsList = helpsList.subList(fromIndex, toIndex);
            } catch (Exception e) {
                telegramAccount.setPage(0);
                telegramAccountService.save(telegramAccount);
            }

            StringBuilder helpersByTown = new StringBuilder();
            int i = 1;
            InlineKeyboardMarkup inlineKeyboardMarkup;
            if (!isLastPage && !isFirstPage) {
                InlineKeyboardButton[][] paginationButtons = new InlineKeyboardButton[1][2];
                paginationButtons[0][0] = new InlineKeyboardButton("\u2190Пред.").callbackData("/decrementPage");
                paginationButtons[0][1] = new InlineKeyboardButton("След.\u2192").callbackData("/incrementPage");
                inlineKeyboardMarkup = new InlineKeyboardMarkup(paginationButtons);
            } else {
                InlineKeyboardButton[][] paginationButtons = new InlineKeyboardButton[1][1];
                paginationButtons[0][0] = isLastPage ? new InlineKeyboardButton("\u2190Пред.").callbackData("/decrementPage") : new InlineKeyboardButton("\u2192След.").callbackData("/incrementPage");
                inlineKeyboardMarkup = new InlineKeyboardMarkup(paginationButtons);
            }
            for (Help n :
                    helpsList) {
//                                sendMessage(h.getName(), chatId, inlineKeyboardMarkup);
                helpersByTown.append(i++)
                        .append(") ")
                        .append(n.getName())
                        .append("\nКонтакты:\n")
                        .append(n.getCanHelpAccount().getFullName())
                        .append("\n")
                        .append(n.getCanHelpAccount().getPhoneNumber())
                        .append("\n------------\n");
            }
            sendMessage(helpersByTown.toString(), Long.parseLong(telegramAccount.getChatId()), inlineKeyboardMarkup);
        }
    }

}
