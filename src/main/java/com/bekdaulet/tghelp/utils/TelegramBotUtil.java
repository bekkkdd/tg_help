package com.bekdaulet.tghelp.utils;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.request.SendPhoto;
import com.pengrad.telegrambot.response.SendResponse;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Component
public class TelegramBotUtil {

    public static boolean saidLastResult = true;

    public SendResponse sendMessage(TelegramBot bot, long chatId, String message) {

        return sendMessage(bot, chatId, message, null, null, null);
    }


    public SendResponse sendMessage(TelegramBot bot, long chatId, String message, boolean html) {
        return sendMessage(bot, chatId, message, html, null, null);
    }

    public SendResponse sendMessage(TelegramBot bot, long chatId, String message, Keyboard replyMarkup) {
        return sendMessageInner(bot, chatId, message, false, false, false, replyMarkup, null, 1);
    }


    public SendResponse sendMessage(TelegramBot bot, long chatId, String message, Keyboard replyMarkup, int replyToMessageId) {
        return sendMessageInner(bot, chatId, message, false, false, false, replyMarkup, replyToMessageId, 1);
    }

    public SendResponse sendMessage(TelegramBot bot, long chatId, String message, Boolean html, Boolean silent, Boolean preview) {
        return sendMessageInner(bot, chatId, message, html, silent, preview, null, null, 1);
    }

    public SendResponse sendMessage(TelegramBot bot,
                                    long chatId,
                                    String message,
                                    Boolean html,
                                    Boolean silent,
                                    Boolean preview,
                                    Keyboard replyMarkup) {
        return sendMessageInner(bot, chatId, message, html, silent, preview, replyMarkup, null, 1);
    }

    public SendResponse sendPhoto(TelegramBot bot,
                                  long chatId,
                                  BufferedImage image,
                                  String message,
                                  int depth) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            SendPhoto sendPhoto = new SendPhoto(chatId, imageInByte).caption(message);
            return bot.execute(sendPhoto);
        } catch (RuntimeException | IOException e) {
            if (depth == 3) {
                try {
                    throw e;
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            return sendPhoto(bot, chatId, image, message, depth + 1);
        }

    }

    private SendResponse sendMessageInner(TelegramBot bot,
                                          long chatId,
                                          String message,
                                          Boolean html,
                                          Boolean silent,
                                          Boolean preview,
                                          Keyboard replyMarkup,
                                          Integer replyToMessageId,
                                          int depth) {
        try {
            SendMessage sendMessage = new SendMessage(chatId, message);
            if (replyToMessageId != null) {
                sendMessage = sendMessage.replyToMessageId(replyToMessageId);
            }
            if (html != null) {
                sendMessage = sendMessage.parseMode(ParseMode.HTML);
            }
            if (silent != null) {
                sendMessage = sendMessage.disableNotification(silent);
            }
            if (preview != null) {
                sendMessage = sendMessage.disableWebPagePreview(preview);
            }
            if (replyMarkup != null) {
                sendMessage = sendMessage.replyMarkup(replyMarkup);
            }
            return bot.execute(sendMessage);
        } catch (RuntimeException e) {
            e.printStackTrace();
            if (depth == 3) {
                throw e;
            }
            return sendMessageInner(bot, chatId, message, html, silent, preview, replyMarkup, replyToMessageId, depth + 1);
        }
    }
    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }


}

