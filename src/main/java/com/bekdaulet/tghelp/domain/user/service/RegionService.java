package com.bekdaulet.tghelp.domain.user.service;

import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionService {
    @Autowired
    RegionRepository regionRepository;

    public List<Region> findAll(){
        return regionRepository.findAll();
    }

    public Region findFirstByName(String name){
        return regionRepository.findFirstByName(name);
    }

    public Region findFirstById(Long id){
        return regionRepository.findFirstById(id);
    }
}
