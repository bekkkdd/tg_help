package com.bekdaulet.tghelp.domain.user.model;

public class UserState {
    public static final String ACTIVE = "ACTIVE";
    public static final String NOT_ACTIVE = "NOT_ACTIVE";
}
