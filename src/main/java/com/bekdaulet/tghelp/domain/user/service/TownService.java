package com.bekdaulet.tghelp.domain.user.service;

import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.model.Town;
import com.bekdaulet.tghelp.domain.user.repository.TownRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TownService {
    @Autowired
    TownRepository townRepository;

    public List<Town> findAll() {
        return townRepository.findAll();
    }

    public List<Town> findAllByRegion(Region region) {
        return townRepository.findAllByRegion(region);
    }

    public Town findFirstByName(String name) {
        return townRepository.findFirstByName(name);
    }

    public Town findFirstById(Long id) {
        return townRepository.findFirstById(id);
    }
}
