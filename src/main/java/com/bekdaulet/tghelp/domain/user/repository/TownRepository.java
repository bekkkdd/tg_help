package com.bekdaulet.tghelp.domain.user.repository;

import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.model.Town;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TownRepository extends CrudRepository<Town, Long> {
    List<Town> findAll();

    List<Town> findAllByRegion(Region region);

    Town findFirstByName(String name);

    Town findFirstById(Long id);
}
