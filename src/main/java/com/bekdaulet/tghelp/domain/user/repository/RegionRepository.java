package com.bekdaulet.tghelp.domain.user.repository;

import com.bekdaulet.tghelp.domain.user.model.Region;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionRepository extends CrudRepository<Region, Long> {
    List<Region> findAll();

    Region findFirstByName(String name);

    Region findFirstById(Long id);
}
