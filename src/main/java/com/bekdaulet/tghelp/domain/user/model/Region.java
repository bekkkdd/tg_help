package com.bekdaulet.tghelp.domain.user.model;

import javax.persistence.*;

@Entity
@Table(name = "region")
public class Region {
    @Id
    @Column(name = "id")
    Long id = null;

    @Column(name = "name")
    String name = null;

    public Region() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
