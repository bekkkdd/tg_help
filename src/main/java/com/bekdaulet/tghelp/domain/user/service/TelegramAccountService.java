package com.bekdaulet.tghelp.domain.user.service;

import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import com.bekdaulet.tghelp.domain.user.repository.TelegramAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TelegramAccountService {
    @Autowired
    TelegramAccountRepository telegramAccountRepository;

    public TelegramAccount findFirstByAccountId(String accountId){
        return telegramAccountRepository.findFirstByAccountId(accountId);
    }

    public List<TelegramAccount> findAll(){
        return telegramAccountRepository.findAll();
    }

    public void save(TelegramAccount account){
        telegramAccountRepository.save(account);
    }
}
