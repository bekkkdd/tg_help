package com.bekdaulet.tghelp.domain.user.model;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Table(name = "telgram_accounts")
@Entity
public class TelegramAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id = null;

    @Column(name = "account_id", unique = true)
    String accountId;

    @Column(name = "chat_id", unique = true)
    String chatId;

    @Column(name = "username")
    String username;

    @Column(name = "full_name")
    String fullName;

    @Column(name = "phone_number")
    String phoneNumber;

    @Column(name = "telegram_status")
    TelegramAccountStatus telegramAccountStatus;

    @Column(name = "is_helper", columnDefinition = "tinyint(1) default 0")
    Boolean isHelper;

    @Column(name = "is_needy", columnDefinition = "tinyint(1) default 0")
    Boolean isNeedy;

    @ManyToOne
    @JoinTable(name = "telegram_account__town",
            joinColumns = @JoinColumn(name = "telegram_account_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "town_id", referencedColumnName = "id")
    )
    Town town;

    @ManyToOne
    @JoinTable(name = "telegram_account__region",
            joinColumns = @JoinColumn(name = "telegram_account_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "region_id", referencedColumnName = "id")
    )
    Region region;



    @ManyToOne
    @JoinTable(name = "telegram_account__search_town",
            joinColumns = @JoinColumn(name = "telegram_account_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "search_town_id", referencedColumnName = "id")
    )
    Town searchTown;

    @ManyToOne
    @JoinTable(name = "telegram_account__search_region",
            joinColumns = @JoinColumn(name = "telegram_account_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "seacrh_region_id", referencedColumnName = "id")
    )
    Region searchRegion;

    @Column(name = "search_String")
    String searchString;

    @Column(name = "page")
            @ColumnDefault("0")
    Integer page = 0;

    @Column(name = "created_time", nullable = false)
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdTime = null;

    @Column(name = "updated_time", nullable = false)
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date updatedTime = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public TelegramAccountStatus getTelegramAccountStatus() {
        return telegramAccountStatus;
    }

    public void setTelegramAccountStatus(TelegramAccountStatus telegramAccountStatus) {
        this.telegramAccountStatus = telegramAccountStatus;
    }

    public Boolean getHelper() {
        return isHelper;
    }

    public void setHelper(Boolean helper) {
        isHelper = helper;
    }

    public Boolean getNeedy() {
        return isNeedy;
    }

    public void setNeedy(Boolean needy) {
        isNeedy = needy;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Town getSearchTown() {
        return searchTown;
    }

    public void setSearchTown(Town searchTown) {
        this.searchTown = searchTown;
    }

    public Region getSearchRegion() {
        return searchRegion;
    }

    public void setSearchRegion(Region searchRegion) {
        this.searchRegion = searchRegion;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
