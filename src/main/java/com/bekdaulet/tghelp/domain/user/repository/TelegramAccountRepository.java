package com.bekdaulet.tghelp.domain.user.repository;

import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TelegramAccountRepository extends CrudRepository<TelegramAccount, Long> {
    TelegramAccount findFirstByAccountId(String accountId);

    List<TelegramAccount> findAll();

//    List<TelegramAccount> findAllByAccountIdBetween(String id, String id2);
}
