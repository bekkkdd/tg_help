package com.bekdaulet.tghelp.domain.goods.repository;

import com.bekdaulet.tghelp.domain.goods.model.Help;
import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import com.bekdaulet.tghelp.domain.user.model.Town;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HelpsRepository extends CrudRepository<Help, Long> {
    List<Help> findAll();

    List<Help> findAllByNameContains(String nameContains);

    List<Help> findAllByCanHelpAccount_Town(Town town);

    List<Help> findAllByCanHelpAccountIsNotAndCanHelpAccount_Town(TelegramAccount account, Town town);

    List<Help> findAllByCanHelpAccount_Region(Region region);

    List<Help> findAllByCanHelpAccount_AccountId(String tg_accountId);

    Help findFirstById(Long id);
}
