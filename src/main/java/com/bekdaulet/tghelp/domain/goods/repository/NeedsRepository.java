package com.bekdaulet.tghelp.domain.goods.repository;

import com.bekdaulet.tghelp.domain.goods.model.Need;
import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import com.bekdaulet.tghelp.domain.user.model.Town;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NeedsRepository extends CrudRepository<Need, Long> {
    List<Need> findAll();

    List<Need> findAllByNameContains(String nameContains);

    List<Need> findAllByNeedHelpAccount_Town(Town town);

    List<Need> findAllByNeedHelpAccountIsNotAndNeedHelpAccount_Town(TelegramAccount account, Town town);

    List<Need> findAllByNeedHelpAccount_Region(Region region);

    List<Need> findAllByNeedHelpAccount_AccountId(String tg_accountId);

    Need findFirstById(Long id);
}
