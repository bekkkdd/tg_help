package com.bekdaulet.tghelp.domain.goods.model;

import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "needs")
@Where(clause="soft_deleted=0")
public class Need {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id = null;

    @Column(name = "name")
    String name = null;

    @OneToOne
    TelegramAccount needHelpAccount = null;

    @Column(name = "soft_deleted", columnDefinition = "tinyint(1) default 0")
    Boolean softDeleted = false;

    public Need() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TelegramAccount getNeedHelpAccount() {
        return needHelpAccount;
    }

    public void setNeedHelpAccount(TelegramAccount needHelpAccount) {
        this.needHelpAccount = needHelpAccount;
    }

    public Boolean getSoftDeleted() {
        return softDeleted;
    }

    public void setSoftDeleted(Boolean softDeleted) {
        this.softDeleted = softDeleted;
    }
}
