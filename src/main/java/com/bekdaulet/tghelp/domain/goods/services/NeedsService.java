package com.bekdaulet.tghelp.domain.goods.services;

import com.bekdaulet.tghelp.domain.goods.model.Need;
import com.bekdaulet.tghelp.domain.goods.repository.NeedsRepository;
import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import com.bekdaulet.tghelp.domain.user.model.Town;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NeedsService {
    @Autowired
    NeedsRepository needsRepository;

    public List<Need> findAll() {
        return needsRepository.findAll();
    }

    public List<Need> findAllByNameContains(String nameContains) {
        return needsRepository.findAllByNameContains(nameContains);
    }

    public List<Need> findAllByNeedHelpAccount_Town(Town town) {
        return needsRepository.findAllByNeedHelpAccount_Town(town);
    }

    public List<Need> findAllByNeedHelpAccountIsNotAndNeedHelpAccount_Town(TelegramAccount account, Town town) {
        return needsRepository.findAllByNeedHelpAccountIsNotAndNeedHelpAccount_Town(account,town);
    }

    public List<Need> findAllByNeedHelpAccount_Region(Region region) {
        return needsRepository.findAllByNeedHelpAccount_Region(region);
    }

    public List<Need> findAllByNeedHelpAccount_AccountId(String tg_accountId) {
        return needsRepository.findAllByNeedHelpAccount_AccountId(tg_accountId);
    }

    public Need findFirstById(Long id) {
        return needsRepository.findFirstById(id);
    }

    public void save(Need need) {
        needsRepository.save(need);
    }
}
