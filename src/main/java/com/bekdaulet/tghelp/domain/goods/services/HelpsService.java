package com.bekdaulet.tghelp.domain.goods.services;

import com.bekdaulet.tghelp.domain.goods.model.Help;
import com.bekdaulet.tghelp.domain.goods.repository.HelpsRepository;
import com.bekdaulet.tghelp.domain.user.model.Region;
import com.bekdaulet.tghelp.domain.user.model.TelegramAccount;
import com.bekdaulet.tghelp.domain.user.model.Town;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelpsService {
    @Autowired
    HelpsRepository helpsRepository;

    public List<Help> findAll() {
        return helpsRepository.findAll();
    }

    public List<Help> findAllByNameContains(String nameContains) {
        return helpsRepository.findAllByNameContains(nameContains);
    }

    public List<Help> findAllByCanHelpAccount_Town(Town town) {
        return helpsRepository.findAllByCanHelpAccount_Town(town);
    }

    public List<Help> findAllByCanHelpAccountIsNotAndCanHelpAccount_Town(TelegramAccount account, Town town) {
        return helpsRepository.findAllByCanHelpAccountIsNotAndCanHelpAccount_Town(account, town);
    }

    public List<Help> findAllByCanHelpAccount_Region(Region region) {
        return helpsRepository.findAllByCanHelpAccount_Region(region);
    }

    public List<Help> findAllByCanHelpAccount_AccountId(String tg_accountId) {
        return helpsRepository.findAllByCanHelpAccount_AccountId(tg_accountId);
    }

    public Help findFirstById(Long id) {
        return helpsRepository.findFirstById(id);
    }

    public void save(Help help) {
        helpsRepository.save(help);
    }
}
