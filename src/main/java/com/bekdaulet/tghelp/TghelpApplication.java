package com.bekdaulet.tghelp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TghelpApplication {
    public static void main(String[] args) {

        SpringApplication.run(TghelpApplication.class, args);

    }
}
